exports.methodTimeout = 1000 * 10; // 10s
exports.totalTimeout = exports.methodTimeout * 6; // 1min

exports.fatalError = (username, error) => {
  console.error(
    `✘ account [${username}] Registration failed, please follow the link instructions to turn off secure defaults (multi-factor authentication)：`,
    'https://docs.microsoft.com/zh-cn/azure/active-directory/fundamentals/concept-fundamentals-security-defaults#disabling-security-defaults',
    error
  );
  process.exit(1);
};
